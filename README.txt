-- SUMMARY --

The Quick Field delete module allows site admin to delete multiple fields
simultaneously in quick and efficient manner on 'Manage field' page.

-- REQUIREMENTS --

field_ui


-- INSTALLATION --

* Install as usual, see http://drupal.org/node/70151 for further information.


-- CONTACT --

Current maintainers:
* Gaurav Pahuja (gaurav.pahuja) - https://drupal.org/user/1804170
* Madhu Sudan (madhusudanmca) - https://www.drupal.org/user/902072/
* Pushpinder Rana (er.pushpinderrana) - https://drupal.org/user/1004418
