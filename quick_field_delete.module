<?php

/**
 * @file
 * Allows multiple fields to be deleted simultaneously in quick and efficient
 * manner on a single page.
 */

/**
 * Implements hook_help().
 */
function quick_field_delete_help($path, $arg) {
  switch ($path) {
    case 'admin/help#quick_field_delete':
      $output = '<p>' . t("The SN Quick Field Delete Module allows site admin to delete multiple fields simultaneously in quick and efficient manner on 'Manage field' page.") . '</p>';
      return $output;
  }
}

/**
 * Implements hook_form_FORM_ID_alter().
 */
function quick_field_delete_form_field_ui_field_overview_form_alter(&$form, &$form_state) {
  // Gather bundle information.
  $instances = field_info_instances($form['#entity_type'], $form['#bundle']);
  $extra_fields = field_info_extra_fields($form['#entity_type'], $form['#bundle'], 'form');

  // Fields.
  foreach ($instances as $name => $instance) {
    $form['fields'][$name]['multi_select'] = array(
      '#type' => 'checkbox',
    );
  }

  // Non-field elements.
  foreach ($extra_fields as $name => $extra_field) {
    $form['fields'][$name]['multi_select'] = array(
      '#markup' => '',
    );
  }

  // Additional row: add new field.
  $form['fields']['_add_new_field']['multi_select'] = array(
    '#type' => 'markup',
  );

  // Check for existing fields.
  $existing_fields = field_ui_existing_field_options($form['#entity_type'], $form['#bundle']);
  if (count($existing_fields) > 0) {
    // Additional row: add existing field.
    $form['fields']['_add_existing_field']['multi_select'] = array(
      '#type' => 'markup',
    );
  }

  $form['fields']['#header'][] = t('Quick Delete');
  $form['actions']['#attributes'] = array('onclick' => 'return (function($){if ($( "input:checked" ).length){var r=confirm("Are you sure you want to delete selected fields?");if (r==true){return true}else{return false;}}return true;})(jQuery);');
  $form['#submit'][] = 'quick_field_delete_multi_delete_submit';
}

/**
 * Submit handler for form field_ui_field_overview_form.
 *
 * @param array $form
 *   Array of form elemnts.
 * @param array $form_state
 *   Array of form state in array format.
 */
function quick_field_delete_multi_delete_submit($form, &$form_state) {
  $fields = $form_state['values']['fields'];

  foreach ($fields as $field_name => $field_values) {

    if (isset($field_values['multi_select']) && $field_values['multi_select']) {
      // Delete here.
      $bundle = $form_state['complete form']['#bundle'];
      $entity_type = $form_state['complete form']['#entity_type'];

      $field = field_info_field($field_name);

      $instance = field_info_instance($entity_type, $field_name, $bundle);

      $bundles = field_info_bundles();
      $bundle_label = $bundles[$entity_type][$bundle]['label'];

      if (!empty($bundle) && $field && !$field['locked']) {
        field_delete_instance($instance);
        drupal_set_message(t('The field %field has been deleted from the %type content type.', array('%field' => $instance['label'], '%type' => $bundle_label)));
      }
      else {
        drupal_set_message(t('There was a problem removing the %field from the %type content type.', array('%field' => $instance['label'], '%type' => $bundle_label)), 'error');
      }

      $admin_path = _field_ui_bundle_admin_path($entity_type, $bundle);
      $form_state['redirect'] = field_ui_get_destinations(array($admin_path . '/fields'));
      // Fields are purged on cron. However field module prevents disabling
      // modules when field types they provided are used in a field until
      // it is fully purged. In the case that a field has minimal or no
      // content, a single call to field_purge_batch() will remove it from
      // the system. Call this with a
      // low batch limit to avoid administrators having to wait for cron runs
      // when removing instances that meet this criteria.
      field_purge_batch(10);
    }
  }
}
